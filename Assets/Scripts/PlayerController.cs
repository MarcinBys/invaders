﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class PlayerController : MonoBehaviour
{
    //private Rigidbody2D rb;

    private Ads ads;
    private bool adShown;
    public Button resurrectButton;

    public static bool lose;
    
    public Text endText;
    public Text scoreText;

    [HideInInspector]public static int score;

    private static int health;
    public Text healthText;

    public GameObject shootObject;
    public GameObject shootPosition;
    private bool canShoot;
    private float shootReloadTimer;
    public float shootReloadTime;
    private AudioSource audioSource;

    void Start()
    {
        shootReloadTimer = 0;
        canShoot = true;

        lose = false;

        endText.text = "";

        score = 0;
        scoreText.text = "Score: 0";

        health = 100;
        healthText.text = "Health: " + health;

        //rb = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();

        ads = FindObjectOfType<Ads>();
        adShown = false;
    }



    void Update()
    {
        if (lose)
        {
            canShoot = false;

            if (Social.localUser.authenticated)
            {
                PlayServices.UnlockAchievement("CgkIrvvlo4wZEAIQCQ");
            }
            endText.text = "You LOSE";
            health = 0;
            UpdateHealth();
            UpdateScore();

            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;

            if (!adShown)
            {
                ads.ShowRewardedAd();
                adShown = true;
            }
        }
        else if (!lose)
        {
            UpdateScore();
            UpdateHealth();
        }

        if (!canShoot)
        {
            shootReloadTimer += Time.deltaTime;

            if (shootReloadTimer >= shootReloadTime)
            {
                canShoot = true;
                shootReloadTimer = 0f;
            }
        }

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (lose)
            {
                /*endText.text = "You LOSE";
                health = 0;
                UpdateHealth();
                UpdateScore();

                gameObject.GetComponent<BoxCollider2D>().enabled = false;
                gameObject.GetComponent<SpriteRenderer>().enabled = false;*/
                
                if (Input.touchCount > 1)
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
            }
        }
    }



    public static void Lose()
    {
        PlayServices.AddScoreToLeaderboard("CgkIrvvlo4wZEAIQBQ", score);

        lose = true;
    }


    public static void AddScore(int points)
    {
        PlayServices.UnlockAchievement("CgkIrvvlo4wZEAIQCw");
        score += points;
    }


    private void UpdateScore()
    {
        scoreText.text = "Score: " + score.ToString();
    }


    public static void Damage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            Lose();
        }
    }

    private void UpdateHealth()
    {
        healthText.text = "Health: " + health.ToString();
    }


    public void Shoot()
    {
        if (!lose && canShoot)
        {
            audioSource.Play();
            Instantiate(shootObject, shootPosition.transform.position, Quaternion.identity);
            canShoot = false;
        }
    }

    public void RessurectPlayer()
    {
        lose = false;
        canShoot = true;

        endText.text = "";

        health = 100;

        gameObject.GetComponent<BoxCollider2D>().enabled = true;
        gameObject.GetComponent<SpriteRenderer>().enabled = true;

        SetActive_ResurrectButton();
    }


    public void SetActive_ResurrectButton()
    {
        resurrectButton.interactable = !resurrectButton.interactable;
    }
}