﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] objectsToSpawn;

    public float timeBetweenSpawn;
    private float timer;

    void Update()
    {
        if (!PlayerController.lose)
        {
            Spawn();
        }
    }

    private void Spawn()
    {
        timer += Time.deltaTime;

        if (timer > timeBetweenSpawn)
        {
            float number = Random.Range(-2.6f, 2.6f);

            int whatToSpawn = Random.Range(0, objectsToSpawn.Length);

            Instantiate(objectsToSpawn[whatToSpawn], transform.position + (Vector3.right * number), Quaternion.identity);

            timer = 0f;
        }
    }
}
