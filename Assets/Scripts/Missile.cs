﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    public float speed;

    private float lifeTime;
    private float time;

    private void Start()
    {
        lifeTime = 5;
    }


    private void Update()
    {
        time += Time.deltaTime;

        transform.Translate(Vector2.up * speed);

        if (time > lifeTime)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            PlayerController.AddScore(10);

            PlayServices.IncrementAchievement("CgkIrvvlo4wZEAIQCg", 50);       // Simple but hidden achievment

            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }
}