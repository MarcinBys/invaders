﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public float speed;

    private float lifeTime;
    private float time;

    private void Start()
    {
        lifeTime = 25;

        speed += (PlayerController.score / 100) * 0.005f;

        if (PlayerController.score >= 200 && PlayerController.score < 300)
        {
            PlayServices.UnlockAchievement("CgkIrvvlo4wZEAIQDA");       // moderate achievement
        }
        else if (PlayerController.score >= 300)
        {
            PlayServices.UnlockAchievement("CgkIrvvlo4wZEAIQDQ");       // hardest achievement
        }

        /*
        if (PlayerController.score >= 100 && PlayerController.score < 200)
        {
            speed += 0.005f;
        }
        else if (PlayerController.score >= 200 && PlayerController.score < 300)
        {
            speed += 0.01f;
            PlayServices.UnlockAchievement("CgkIrvvlo4wZEAIQDA");       // moderate achievement
        }
        else if (PlayerController.score >= 300)
        {
            speed += 0.015f;
            PlayServices.UnlockAchievement("CgkIrvvlo4wZEAIQDQ");       // hardest achievement
        }*/
    }


    private void Update()
    {
        time += Time.deltaTime;

        transform.Translate(Vector2.down * speed);
        
        if (time > lifeTime)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerController.Damage(20);
            Destroy(gameObject);
        }
        else if (collision.gameObject.CompareTag("LoseBorder"))
        {
            PlayerController.Lose();
            Destroy(gameObject);
        }
    }
}